# -*- makefile -*-
NAME = "Settings Display Sneak Peak"
SUMMARY = "Add Sneak Peak option to displas settings"
DESCRIPTION := "Adds an option for Sneak Peek in Display settings whithout any need to mce-tools. In fact, there is a built-in option for it disabled by default; this patch re-enables it."
LICENSE=GPLv2+
VER=3.0.1.14
MAINTAINER=Thaodan
# see https://github.com/sailfishos-patches/patchmanager#categories
CATEGORY=homescreen
